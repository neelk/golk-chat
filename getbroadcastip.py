import socket
import os

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("0.0.0.0", 4567))
while True:
    data, addr = client.recvfrom(1024)
    print("received message: %s"%data)
    print(addr)
    # open browser to chat addr here
    os.system("firefox http://%s:1211" % (addr[0]))
    if(len(data) > 0):
        exit(0)