import socket
from time import sleep

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.bind(("127.0.0.1", 44444))

while True:
    sock.sendto(b"hello from PYTHON", ('<broadcast>', 4567))
    sleep(1)

sock.close()