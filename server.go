package main


import (

    "fmt"
    "log"
    "os"
    "sync"

    "strings"
    "net/http"

    "github.com/gorilla/websocket"

)


type message struct {

    source string
    value string

}


type user struct {

    uname string
    /*
        "channel0" for sending to server
        "channel1" for receiving from server
    */
    utype string
    getchan chan message
    readconn *websocket.Conn
    writeconn *websocket.Conn

}


var wg sync.WaitGroup


var users map[int]user
var uids []int
var n_users int
var users_mutex sync.Mutex


var putchan = make(chan message)


var upgrader = websocket.Upgrader {
                   EnableCompression: true,
               }


/*
func remove(s []user, i int) {

    // Deletes the element at index i from a slice

    s[i] = s[len(s)-1]
    // return s[:len(s)-1]

}
*/


func checkorigin(r *http.Request) bool {

    return true;

}


func getmessages(this_user_no int, putchan chan message) {

    for {

        // fmt.Println(users[this_user_no])
        msgType, msg, err := users[this_user_no].readconn.ReadMessage()
        if err != nil {
            users[this_user_no].readconn.Close()
            users_mutex.Lock()
            delete(users, this_user_no)
            users_mutex.Unlock()
            log.Print(err)
            break
        }

        if msgType == 1 {
            putchan <- message {source: users[this_user_no].uname,
                                value: strings.TrimSpace(string(msg))}
        }

    // TODO: Put the received message into a database

    }

}


func putmessages(this_user_no int, getchan chan message) {

    /*
        Message format:
            Line 1: Source Username
            Line 2: Number of lines in message
                        > Messages must have a newline terminating the
                          string so that string.Count(msg.value, "\n")
                          is equal to the number of lines in the
                          message
            Line 2: Message
    */

    // TODO: read message from database

    for msg := range getchan {

        err := users[this_user_no].writeconn.WriteMessage(1,
                   []byte(fmt.Sprintf("%s\n%d\n%s\n", msg.source,
                                   strings.Count(msg.value, "\n"),
                   msg.value)))
        if err != nil {
            users[this_user_no].writeconn.Close()
            users_mutex.Lock()
            delete(users, this_user_no)
            users_mutex.Unlock()
            log.Print(err)
            break
        }

    }

}


func chathandler_clientrecv(w http.ResponseWriter, r *http.Request) {

    /*
        type: channel1
        Initializes websocket for writing to the client
        msgType:
            0: Normal message
    */

    var name string
    var temp []byte
    var new_user_id int

    upgrader.CheckOrigin = checkorigin;

    writeconn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Print(err)
        return
    } else {
        // fmt.Println(writeconn)
        _, temp, err = writeconn.ReadMessage()
        if err != nil {
            log.Print(err)
            return
        }
        name = string(temp)
    }

    users_mutex.Lock()
    new_user_id = n_users
    users[n_users] = user {uname: name,
                     utype: "channel1",
                     getchan: make(chan message),
                     writeconn: writeconn}
    n_users++
    users_mutex.Unlock()

    wg.Add(1)
    go putmessages(new_user_id, users[new_user_id].getchan)
    wg.Done()

    wg.Wait()

}


func chathandler_clientsend(w http.ResponseWriter, r *http.Request) {

    /*
        type: channel0
        Initializes a websocket for reading from the client
        msgType:
            1: Normal message
    */

    var name string
    var temp []byte
    var new_user_id int

    // fmt.Println(r)
    upgrader.CheckOrigin = checkorigin;

    readconn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Print(err)
        return
    } else {
        // fmt.Println(readconn)
        _, temp, err = readconn.ReadMessage()
        if err != nil {
            log.Print(err)
            return
        }
        name = string(temp)
    }

    users_mutex.Lock()
    new_user_id = n_users
    users[n_users] = user {uname: name,
                           utype: "channel0",
                           getchan: make(chan message),
                           readconn: readconn}
    n_users++
    users_mutex.Unlock()

    wg.Add(1)
    go getmessages(new_user_id, putchan)
    wg.Done()

    wg.Wait()

}


func transmitter() {

    for msg := range putchan {

        // fmt.Println("got message", len(users))

        // for i := 0; i < len(users); i++ {
        for _, val := range users {

            if val.utype == "channel1" {
                // fmt.Println("sending", string(msg.value),
                // "to", val.uname, "uid", uid)
                val.getchan <- msg
            }

        }

    }

}


func main() {

    users = make(map[int]user)

    // server is receiving: channel0
    http.HandleFunc("/channel0", chathandler_clientsend)

    // server is sending: channel1
    http.HandleFunc("/channel1", chathandler_clientrecv)

    http.HandleFunc("/",
             func (w http.ResponseWriter, r *http.Request) {
                 http.ServeFile(w, r, "html/html/chat.html")
             })

    http.HandleFunc("/chat.css",
             func (w http.ResponseWriter, r *http.Request) {
                 http.ServeFile(w, r, "html/css/chat.css")
             })

    http.HandleFunc("/chat.js",
             func (w http.ResponseWriter, r *http.Request) {
                 http.ServeFile(w, r, "html/js/chat.js")
             })

    go transmitter()

    fmt.Println("starting server")

    log.Fatal(http.ListenAndServe(":1211", nil))

    os.Exit(0)

}
