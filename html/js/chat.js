function HtmlEncode(s) {
    var el = document.createElement("div");
    el.innerText = el.textContent = s;
    s = el.innerHTML;
    return s;
}

function gen_message_element(sender, contents) {

    var msg_ele = document.createElement('div');
    msg_ele.setAttribute('class', 'message');

    var fldst = document.createElement('fieldset');
    fldst.innerHTML = '';
    var legend = document.createElement('legend');
    legend.innerHTML = sender;
    fldst.appendChild(legend);
    fldst.innerHTML += contents;

    msg_ele.appendChild(fldst);

    if (sender === name) {

        msg_ele.setAttribute('style',
            msg_ele.getAttribute('style') + 'margin-right: 3%;')

    }

    return msg_ele;

}

var input = document.getElementById("input");
var output = document.getElementById("output");
var send_socket;
var recv_socket;

var n_calls = 0;

var name = window.prompt("Enter your name");
init();

function init () {

    n_calls += 1;
    if (n_calls == 10) {
        while (true) {
            ;
        }
    }
    send_socket = new WebSocket("ws://" + document.domain + ":1211/channel0");
    recv_socket = new WebSocket("ws://" + document.domain + ":1211/channel1");

    send_socket.onclose = function (e) {
        console.log('closing send socket, attempting to reopen');
        init();
    }

    recv_socket.onclose = function (e) {
        console.log('closing recv socket, attempting to reopen');
        init();
    }

    send_socket.onerror = function (e) {
        console.log(e);
        send_socket.close();
        // send_socket = new WebSocket("ws://" + document.domain + ":1211/channel0");
        init();
    };
    recv_socket.onerror = function (e) {
        console.log(e);
        recv_socket.close();
        // recv_socket = new WebSocket("ws://" + document.domain + ":1211/channel1");
        init();
    };

    send_socket.onopen = function onopen_send(openevent) {
        output.innerHTML += "Status: Sending Socket Connected\n";
        console.log("Status: Sending Socket Connected");
        send_socket.send(name);
    };
    recv_socket.onopen = function onopen_recv(openevent) {
        output.innerHTML += "Status: Receiving Socket Connected\n";
        console.log("Status: Receiving Socket Connected");
        recv_socket.send(name);
    };
}

/* socket.onmessage = function (e) {
    output.innerHTML += "Server: " + e.data + "\n";
}; */

function send() {
   send_socket.send(input.value);
   input.value = "";
   input.focus();
}

recv_socket.onmessage =
    function (msgevent) {

        // console.log(msgevent);
        var msg = msgevent.data;
        var l = msg.split("\n");
        // output.innerHTML += msg;
        /*
        output.innerHTML += (l[0] + "\n");
        output.innerHTML += (l.slice(2).join("\n") + '\n');
        */

        // var sender = (l[0] + "\n"); // with terminating newline
        var sender = (l[0]); // without terminating newline
        var msg_contents = (l.slice(2).join("\n") + '\n');

        var msg_ele = gen_message_element(sender, HtmlEncode(msg_contents));

        console.log('appending');
        output.appendChild(msg_ele);

        window.scrollTo(0, document.body.scrollHeight);

    };
